﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoomGenerator : MonoBehaviour {
    public enum RoomType
    {
        //fill this up later with types of rooms
        Food, Sentry, Maze, Switches, Saw,
    }
    [Header("Prefabs")]
    public GameObject floorTile;
    public GameObject door;
    public GameObject[] wallTiles;
    public GameObject foodSpawner;
    public GameObject saw;
    public List<GameObject[]> mazes; //this will probably have to be a 2D array or a list of arrays to account for levels (level 1 has 10 different mazes, level 2 has 10 different (harder) mazes, etc.)
    public GameObject[] switches1, switches2, switches3, switches4; //this is the basic way to do it but may be the best for dragging prefabs in editor
    public GameObject[] sawPatterns1, sawPatterns2, sawPatterns3, sawPatterns4;

    //instances of prefabs:
    GameObject fs_i, sentry_i, maze_i, switches_i, saw_i;
    
    public int roomWidth, roomHeight;
    public int roomID;
    Vector2 NSDoorSpot, EWDoorSpot; //store thedoor coords so that a wall won't be spawned there.
    public RoomType roomType;
    public float roomLightsDelay = .002f;

    public bool playerIsInMe = false;
    public bool lightsOn;
    public bool hasFood, hasSentry, hasMaze, hasSwitches, hasSaw;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
    //~later this should also take in RoomType as a param
    public void SetupRoom(int width, int height)
    {
        //add yourself to the zone list of rooms (so you can be accessed by other rooms and can talk to each other!)
        BoardManager.BM.rooms.Add(gameObject);
        roomID = BoardManager.BM.rooms.IndexOf(gameObject);

        //store height and width so it can be used by other things
        roomWidth = width;
        roomHeight = height; 

        //get the door pos from the previous room:

        SpawnADoor(width, height);
        for (int i=0; i<width; i++)
        {
            for(int j=0; j<height; j++)
            {
                //first, drop a floor everywhere:
                GameObject floorTilei = Instantiate(floorTile, new Vector2(transform.position.x + i, transform.position.y + j), Quaternion.identity) as GameObject;
                floorTilei.transform.parent = gameObject.transform; //make it a child of this room.

                //trhen drop walls around the perimeter of the room (unless it's a designated door spot):
                if (i == 0 || i == width-1 || j == 0 || j == height-1)
                {
                    Vector2 currentPos = new Vector2(i, j);
                    if (currentPos != NSDoorSpot && currentPos != EWDoorSpot && currentPos!= new Vector2(ConnectedRoomNDoorSpot().x, 0) && currentPos != new Vector2(0, ConnectedRoomEDoorSpot().y))
                    {
                        GameObject wall = Instantiate(wallTiles[Random.Range(0, wallTiles.Length)], new Vector2(transform.position.x + i, transform.position.y + j), Quaternion.identity) as GameObject;
                        wall.transform.parent = gameObject.transform; //make it a child of this room.
                    }

                }

              
            }
        }
        //then, check what type of room you are and do some stuff accordingly.
        //Debug.Log("my type = " + roomType);
        switch (roomType)
        {
            case RoomType.Food:
                InstantiateFoodSpawner(transform.position);
                hasFood = true;
                break;
            case RoomType.Switches:
                InstantiateSwitchPattern(transform.position);
                hasSwitches = true;
                break;
            case RoomType.Saw:
                InstantiateSawObstacle(transform.position);
                hasSaw = true;
                break;
        }
    }


    //this seems like it could get hairy so i'm putting it in a function all of its own:
    void SpawnADoor(int width, int height)
    {
        int randomNSDoorX = Random.Range(1, width - 1); //pick a random number from 1 (within the room) to 1 less than the width
        int randomEWDoorY = Random.Range(1, height - 1);
        //bottom row north doors:
        if (roomID % GridDungeonCreator.GDC.numRoomsSquared == 0) //if it's a clean factor of the number of rooms (ex. 5x5 should make northern walls only at 5, 10, 15, etc. because they're on the bottom of the map)
        {
            GameObject firstNDoor = Instantiate(door, new Vector2(transform.position.x + randomNSDoorX, transform.position.y + height - 1), Quaternion.identity) as GameObject;
            firstNDoor.transform.parent = gameObject.transform;
            NSDoorSpot = new Vector2(randomNSDoorX, height - 1);
        }
        else
        {
            //at this point, just spawn a door at the top of every room.  ~~will have to change to account for top row not allowing north doors:
            GameObject otherNDoor = Instantiate(door, new Vector2(transform.position.x + randomNSDoorX, transform.position.y + height - 1), Quaternion.identity) as GameObject;
            otherNDoor.transform.parent = gameObject.transform;
            NSDoorSpot = new Vector2(randomNSDoorX, height - 1);

        }

        GameObject eastDoor = Instantiate(door, new Vector2(transform.position.x + width - 1, transform.position.y + randomEWDoorY), Quaternion.identity) as GameObject;
        eastDoor.transform.parent = gameObject.transform;
        EWDoorSpot = new Vector2(width - 1, randomEWDoorY);
        //lock each door if it's a switch room:
        if(roomType == RoomType.Switches)
        {
            foreach(Door dr in gameObject.GetComponentsInChildren<Door>())
            {
                dr.LockMe();
            }
        }
    }

    public void UnlockDoors()
    {
        foreach(Door dr in gameObject.GetComponentsInChildren<Door>())
        {
            dr.UnlockMe();
        }
    }

    public Vector2 ConnectedRoomNDoorSpot()
    {
        Vector2 lastRoomSpot = Vector2.zero;
        if (roomID > 0)
        {
            lastRoomSpot = BoardManager.BM.rooms[roomID - 1].GetComponent<RoomGenerator>().NSDoorSpot;
        }
        return lastRoomSpot;

    }

    public Vector2 ConnectedRoomEDoorSpot()
    {
        Vector2 lastRoomSpot = Vector2.zero;
        if (roomID > GridDungeonCreator.GDC.numRoomsSquared-1)
        {
            lastRoomSpot = BoardManager.BM.rooms[roomID - GridDungeonCreator.GDC.numRoomsSquared].GetComponent<RoomGenerator>().EWDoorSpot;
        }
        return lastRoomSpot;

    }

    void InstantiateFoodSpawner(Vector2 myRoomPos)
    {
        fs_i = Instantiate(foodSpawner, myRoomPos, Quaternion.identity) as GameObject;
        fs_i.GetComponent<SpawnFood>().borderRight = roomWidth;
        fs_i.GetComponent<SpawnFood>().borderTop = roomHeight;
        fs_i.transform.parent = gameObject.transform;//child it to this room so that the light switch can turn it on?
        if (!playerIsInMe)
        {
            //fs_i.GetComponent<SpawnFood>().enabled = false;
            fs_i.SetActive(false);
        }
        //fs.transform.parent = GameObject.Find("FoodSpawnHolder").transform;
    }

    public void InstantiateSwitchPattern(Vector2 roomPos)
    {
        switch (GameManager.instance.currentLevel)
        {
            case 0:
            case 1:
                GameObject sp = Instantiate(switches1[Random.Range(0, switches1.Length)], new Vector2(roomPos.x, roomPos.y), Quaternion.identity) as GameObject;
                sp.transform.parent = gameObject.transform;
                break;

        }
    }
    
    public void InstantiateSawObstacle(Vector2 roomPos)
    {
        //int randX = (int)Random.Range(roomPos.x+1, roomWidth-1);
        int randY = (int)Random.Range(roomPos.y + 1, roomPos.y+(roomHeight - 1));
        saw_i = Instantiate(saw, new Vector2(roomPos.x+1, randY), Quaternion.identity) as GameObject;
        saw_i.transform.parent = gameObject.transform;
        switch (GameManager.instance.currentLevel)
        {
            case 0:
            case 1:
                //instantiate pattern at same point
                GameObject sPat = Instantiate(sawPatterns1[Random.Range(0, sawPatterns1.Length)], new Vector2(roomPos.x+1, randY), Quaternion.identity) as GameObject;
                sPat.transform.parent = gameObject.transform;
                //also set the saw's path to this so it knows what to follow.
                saw_i.GetComponent<PathFollow>().path = sPat.transform;
                break;

        }
        saw_i.GetComponent<PathFollow>().AssignPathPoints();
        if (!playerIsInMe)
        {
            saw_i.SetActive(false); //~i don't think i have to set the path inactive, too.  it's just sitting there...
        }
    }

    public void TurnOn()
    {
        //first, switch on all the lights:
        playerIsInMe = true;
        if (!lightsOn) //if the lights haven't been turned on yet, go ahead and do that:
        {
            StartCoroutine(SwitchOnLights(roomLightsDelay));
            GameManager.instance.roomsDiscovered++;
        }
        lightsOn = true;
        
        if (hasFood)
        {
            //fs_i.GetComponent<SpawnFood>().enabled = true;
            fs_i.SetActive(true);
        }
        if (hasSaw)
        {
            saw_i.SetActive(true);
        }
    }

    IEnumerator SwitchOnLights(float delay)
    {
        //foreach (SpriteRenderer sr in GetComponentsInChildren<SpriteRenderer>())
        //{
        //    sr.color = Color.white;
        //    //yield return new WaitForSeconds(delay);
        //    yield return new WaitForSecondsRealtime(delay);
        //}
        for(int i=0; i<GetComponentsInChildren<SpriteRenderer>().Length; i++)
        {
            GetComponentsInChildren<SpriteRenderer>()[i].color = Color.white;
            //yield return new WaitForSecondsRealtime(delay); //apparently waitforseconds has a lower limit that is dependent on framerate.  may want to change this ~~!
            //yield return new WaitForEndOfFrame();
        }
        yield return null; //this is the code for no delay.

    }

    public void TurnOff()
    {
        playerIsInMe = false; //tracking this will help turn stuff off that's not near the player (i think)
        //Debug.Log("lights out");
        if (hasFood)
        {
            //fs_i.GetComponent<SpawnFood>().enabled = false;
            fs_i.SetActive(false); //this currently isn't clearning the pellets that have already been spawned.  ~not sure if i want to do that or not.

        }
        if (hasSaw)
        {
            saw_i.SetActive(false);
        }
    }

}
