﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwitchPatternScript : MonoBehaviour {
    public Sprite[] switchSprites; //0 = off, 1 = on
    public bool imOn;
    public SpriteRenderer sr; //was getting an inconsistent NRE from the player spawning in this beffore it was assigned, so, going with assigning in editor
    public SwitchCheck parentChecker;
	// Use this for initialization
	void Awake () {
        sr = GetComponent<SpriteRenderer>();
        parentChecker = transform.parent.GetComponent<SwitchCheck>();
    }

    // Update is called once per frame
    void Update () {
		
	}

    void OnTriggerEnter2D(Collider2D col)
    {
        switch (col.tag)
        {
            case "Player":
                sr.sprite = switchSprites[1];
                parentChecker.CheckIfAllOn();
                imOn = true;

                break;

        }
    }
    void OnTriggerStay2D(Collider2D col)
    {
        switch (col.tag)
        {
            case "Player":
            case "Tail":
                
                imOn = true;
                if (sr.sprite != switchSprites[1])
                {
                    sr.sprite = switchSprites[1];
                }
                break;
        }
    }

    void OnTriggerExit2D(Collider2D col)
    {
        switch (col.tag)
        {
            case "Player":
            case "Tail":
                imOn = false;
                if (sr.sprite != switchSprites[0])
                {
                    sr.sprite = switchSprites[0];
                }
                parentChecker.CheckIfAllOn();

                break;
        }
    }

}
