﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DieTimerScript : MonoBehaviour {
    public float dieAfter;
	// Use this for initialization
	void Start () {
        Invoke("Die", dieAfter);
	}

    void Die()
    {
        Destroy(transform.parent.gameObject);
    }
}
