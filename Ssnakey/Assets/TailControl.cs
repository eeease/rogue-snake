﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TailControl : MonoBehaviour {
    [Header("Modifiers")]
    public bool isBomb = false;
    public bool isTurret = false;

    [Header("Timing")]
    public float explosionDelay = 1f;
    public float explosionDelayOG;
    public float turretShotDelay = 1f;
    public float turretShotDelayOG;
    public GameObject explosionParticles;

    public GameObject backgroundBorder; //going to destroy these at end of run for ghost dead tail effect
    Rigidbody2D myRB;
    BoxCollider2D myBC;
    public SpriteRenderer mySR;
    public int myTailIndex = 0; //what place are you in the snake's tail?
    public SnakeControl myHead; //assigned by head upon instantiation

	// Use this for initialization
	void Start () {
        myRB = GetComponent<Rigidbody2D>();
        myBC = GetComponent<BoxCollider2D>();
        mySR = GetComponent<SpriteRenderer>();
	}
	
	// Update is called once per frame
	void Update () {
        if (isBomb)
        {
            BombLogic();
        }

        if (isTurret)
        {
            TurretLogic();
        }
		
	}

    void BombLogic()
    {
        explosionDelay -= Time.deltaTime;
        if(explosionDelay <= 0)
        {
            BlowUp();
            isBomb = false; //not sure if this is necessary
        }
    }

    void BlowUp()
    {
        Instantiate(explosionParticles, transform, false);
        //the explosionparticles will kill this (their parent)
    }

    void TurretLogic()
    {
        turretShotDelay -= Time.deltaTime;
        if (turretShotDelay <= 0)
        {
            ShootBullet(GameManager.ShotType.Regular);
            turretShotDelay = turretShotDelayOG;
        }
    }

    void ShootBullet(GameManager.ShotType shot)
    {

    }

    public void GhostLikeSwayze()
    {
        GameManager.instance.ghostedTails.Add(gameObject); //add yourself to the GM list of ghosted tails (to be trashed when going to title)
        Destroy(backgroundBorder);
        Destroy(myRB);
        Destroy(myBC);
        Color startCol = mySR.color;
        mySR.color = new Color(startCol.r, startCol.g, startCol.b, .2f);
    }


    void OnTriggerEnter2D(Collider2D col)
    {
        if(col.tag == "Saw")
        {
            myHead.GhostTails(myTailIndex);
            col.GetComponent<Collider2D>().enabled = false;
            col.GetComponent<PathFollow>().reenableCollider = true;

        }
    }
}
