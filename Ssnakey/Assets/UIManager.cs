﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour {
    public static UIManager UI;

    [Header("Canvases")]
    public GameObject overlayCan;

    [Header("Major Panels")]
    public GameObject titlePanel;
    public GameObject mainGamePanel;
    public GameObject scoreScreenPanel;

    public Text swipeButtText;
    [Header("Sub Panels")]
    public GameObject numpadPanel;
    public GameObject minimapPanel;

    [Header("Misc.")]
    public Animator anim;

    [Header("Score Texts")]
    public Text segmentsConsumed;
    public Text roomsDiscovered;
    public Text maxLength;
    public Text totalScore;

    void Awake()
    {
        if (UI == null)
        {
            UI = this;
        }
        else if (UI != this)
        {
            Destroy(gameObject);
        }
        DontDestroyOnLoad(gameObject);
    }
	// Use this for initialization
	void Start () {
        overlayCan = GameObject.Find("OverlayCanvas");
        anim = overlayCan.GetComponent<Animator>();

        foreach (Transform go in overlayCan.GetComponentsInChildren<Transform>())
        {
            switch (go.name)
            {
                case "TitleScreenPanel":
                    titlePanel = go.gameObject;
                    break;

                case "MainGameBlank":
                    mainGamePanel = go.gameObject;
                    break;

                case "ScorePanel":
                    scoreScreenPanel = go.gameObject;
                    break;

                case "NumPadPanel":
                    numpadPanel = go.gameObject;
                    break;

                case "MiniMapPanel":
                    minimapPanel = go.gameObject;
                    break;

                case "SegmentsConsumed":
                    segmentsConsumed = go.GetComponent<Text>();
                    break;

                case "RoomsDiscovered":
                    roomsDiscovered = go.GetComponent<Text>();
                    break;

                case "MaxLength":
                    maxLength = go.GetComponent<Text>();
                    break;

                case "TotalScore":
                    totalScore = go.GetComponent<Text>();
                    break;
                case "SwipeSwapButtText":
                    swipeButtText = go.GetComponent<Text>();
                    break;

            }
        }
	}
	
	// Update is called once per frame

    /// <summary>
    /// which panel to center on? 0=titleFromScore, 1=mainFromTitle, 2 = scoreFromMain
    /// </summary>
    /// <param name="panelNum"></param>
    public void LerpPanel(int panelIndex)
    {
        anim.SetInteger("panelIndex", panelIndex);

        //set the GM bools here, too, i guess:
        //~i could set up the animator parameter index to make these switch cases neatly fall into each other
        switch (panelIndex)
        {
            case 0:
                GameManager.instance.SetGameStateBools(0); //inTitle is true here
                break;

            case 1:
                GameManager.instance.SetGameStateBools(1); //inRun is true here

                break;

            case 2:
                GameManager.instance.SetGameStateBools(2); //inScore is true here
                CalculateScore();
                break;

            case 3:

                break;

            case 4:
                GameManager.instance.SetGameStateBools(1);
                break;

            case 5:

                break;
        }
    }

    //tbh not sure if this should be done here or GM
    public void CalculateScore()
    {
        segmentsConsumed.text = GameManager.instance.segmentsConsumed.ToString();
        roomsDiscovered.text = GameManager.instance.roomsDiscovered.ToString();
        maxLength.text = GameManager.instance.maxLength.ToString();
        int total = 0;
        total = ((GameManager.instance.segmentsConsumed) + (GameManager.instance.roomsDiscovered * GameManager.instance.maxLength));
        totalScore.text = total.ToString();


    }
}
