﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SpriteSwap : MonoBehaviour {
    Image sr;
    public Sprite[] sprites;
    int index = 0;

    public float swapAfter = .5f;
	// Use this for initialization
	void Start () {

        sr = GetComponent<Image>();
        InvokeRepeating("SwapIt", swapAfter, swapAfter);

	}

    void SwapIt()
    {
        if (index < sprites.Length - 1)
        {
            index++;
        }else
        {
            index = 0;
        }
        sr.sprite = sprites[index];
    }
	
	
}
