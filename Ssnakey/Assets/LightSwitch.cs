﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//this is a simple helper obj childed to each room.  when player collides with it, give parent room that info.
public class LightSwitch : MonoBehaviour {
    BoxCollider2D myCol;
    public RoomGenerator parentRoom;
	// Use this for initialization
	void Start () {
        myCol = GetComponent<BoxCollider2D>();
        parentRoom = GetComponentInParent<RoomGenerator>();
        myCol.size = new Vector2(parentRoom.roomWidth-1, parentRoom.roomHeight-1);
        transform.localPosition = new Vector2(parentRoom.roomWidth / 2, parentRoom.roomHeight / 2);
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    void OnTriggerEnter2D(Collider2D col)
    {
        if(col.tag == "Player")
        {
            parentRoom.TurnOn();
        }
    }

    void OnTriggerExit2D(Collider2D col)
    {
        //~!Ok, the issue here seems to be that the parent's (generic room walls) collider is triggering this to happen...
        if(col.tag == "Player")
        {
            if(!GameManager.instance.runOver) //don't pan the camera if you just crashed... into me... babay.
            //since this always seems to trigger calling it from snake control does not seem to trigger consistently, i'm calling it here.
            Camera.main.GetComponent<CamControl>().LerpToNextRoom(col.GetComponent<SnakeControl>().dir);

            parentRoom.TurnOff();
        }
    }
}
