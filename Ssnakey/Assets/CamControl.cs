﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CamControl : MonoBehaviour
{
    BoxCollider2D myCol;

    public int colliderWidth, colliderHeight;
    public float lerpSpeed;
    public bool lerping;
    public float distance;
    public Vector3 destinationPoint;


    // Use this for initialization
    void Start()
    {
        colliderWidth = GridDungeonCreator.GDC.roomWidth;
        colliderHeight = GridDungeonCreator.GDC.roomHeight;
        myCol = GetComponent<BoxCollider2D>();
        myCol.isTrigger = true;
        //~~Need to figure out how to modify the bounds of a collider.

        myCol.size = new Vector2(colliderWidth * 2, colliderHeight * 2); //this probably isn't the best width/height just yet but i'm going for it.
    }

    // Update is called once per frame
    void Update()
    {
        if (lerping)
        {
            if (Vector3.Distance(transform.position, destinationPoint) > distance)
            {
                transform.position = Vector3.Lerp(transform.position, destinationPoint, Time.deltaTime * lerpSpeed);
            }
            else
            {
                transform.position = destinationPoint; //hard set its position after lerping close to it.
                lerping = false;
                myCol.enabled = true;

            }
        }

    }

    public void LerpToNextRoom(Vector2 playerDir)
    {
        myCol.enabled = false;
        if (playerDir == Vector2.right)
        {
            //get x pos of room to right:
            float rightRoomX = transform.position.x + (GridDungeonCreator.GDC.roomWidth);
            destinationPoint = new Vector3(rightRoomX, transform.position.y, -1);
        }
        else if (playerDir == Vector2.left)
        {
            float leftRoomX = transform.position.x - (GridDungeonCreator.GDC.roomWidth);
            destinationPoint = new Vector3(leftRoomX, transform.position.y, -1);
        }
        else if (playerDir == Vector2.up)
        {
            float upRoomY = transform.position.y + (GridDungeonCreator.GDC.roomHeight);
            destinationPoint = new Vector3(transform.position.x, upRoomY, -1);
        }
        else if (playerDir == Vector2.down)
        {
            float downRoomY = transform.position.y - (GridDungeonCreator.GDC.roomHeight);
            destinationPoint = new Vector3(transform.position.x, downRoomY, -1);
        }
        lerping = true;

    }

    void OnTriggerExit2D(Collider2D col)
    {
        //Vector2 dir = col.GetComponent<SnakeControl>().dir;
        //myCol.enabled = false;

        //if (!lerping)
        //{
        //    switch (col.tag)
        //    {
        //        case "Player":
        //            //might have to do some contacts work to check which side the player is moving to.
        //            //for now just check dir
        //            LerpToNextRoom(dir);
        //            break;
        //    }
        //}
    }
}
