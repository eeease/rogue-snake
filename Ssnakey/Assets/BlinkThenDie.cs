﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlinkThenDie : MonoBehaviour {
    SpriteRenderer sr;
    public float startBlinkingAfter, blinkTime, blinkTimeOG, lastBlinkThreshold;
	// Use this for initialization
	void Start () {
        sr = GetComponent<SpriteRenderer>();
	}
	
	// Update is called once per frame
	void Update () {
        startBlinkingAfter -= Time.deltaTime;

        if (startBlinkingAfter <= 0)
        {
            Blink();
            //start counting down to the final blink:
            lastBlinkThreshold -= Time.deltaTime;
            if (lastBlinkThreshold <= 0)
            {
                Destroy(gameObject);
            }
        }
	}
    public void Blink()
    {
        blinkTime -= Time.deltaTime;
        if (blinkTime <= 0)
        {
            if (sr.enabled && lastBlinkThreshold > .5) //for the last one, stay enabled
            {
                sr.enabled = false;
            }
            else
            {
                sr.enabled = true;
            }
            blinkTime = blinkTimeOG;
        }
    }

}
