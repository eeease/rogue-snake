﻿//using System.Collections;
//using System.Collections.Generic;
//using UnityEngine;

//public class BoardCreatorNEW : MonoBehaviour
//{
//    //This edited version of the BaordCreator will spawn all the rooms in a grid.
//    public enum TileType //first make them walls or floors or doors.
//    {
//        Wall, Floor, Door,
//    }

//    public int columns = 100; //total board size
//    public int rows = 100;
//    //helper class that generates random numbers.
//    public int numRooms = 4; //doesn't have to be a range anymore if I'm doing a square tile.
//    public int roomWidth = 9;
//    public int roomHeight = 9;
//    public int corridorLength = 3;
//    public GameObject[] floorTiles;
//    public GameObject[] wallTiles;
//    public GameObject[] outerWallTiles;
//    public GameObject regularDoor;
//    public GameObject foodSpawner;

//    private TileType[][] tiles; //a jagged array of TileTypes
//    private RoomNEW[,] rooms;
//    public CorridorNEW[] corridorsN, corridorsE;
//    private GameObject boardHolder;

//    public GameObject player;
//    // Use this for initialization
//    void Start()
//    {

//        boardHolder = new GameObject("BoardHolder");

//        SetupNewBoard();

//    }

//    public void SetupNewBoard()
//    {
//        SetupTilesArray();

//        CreateRoomsAndCorridors();

//        SetTilesValuesForRooms();
//        SetTilesValuesForCorridors();

//        InstantiateTiles();
//        InstantiateOuterWalls();
//        if (GameManager.instance.levelImage.activeSelf)
//        {
//            GameManager.instance.levelImage.SetActive(false);
//        }
//    }

//    void SetupTilesArray()
//    {
//        //set the tiles jagged array to the correct width:
//        tiles = new TileType[columns][];

//        //go through all the tile arrays:
//        for (int i = 0; i < tiles.Length; i++)
//        {
//            //set each tile array to correct height:
//            tiles[i] = new TileType[rows];
//        }
//    }

//    void CreateRoomsAndCorridors()
//    {
//        //Create the rooms array with a random size.
//        rooms = new RoomNEW[numRooms,numRooms]; //num rooms is an intrange, so saying .random means give me a number between 15 and 20 (in this ex)

//        //There should be a northbound column and an eastbound column per room
//        corridorsN = new CorridorNEW[numRooms*(numRooms)]; //one less corridor than room (ex 3x3 grid of rooms needs 6 northbound and 6 eastbound corridors.
//        corridorsE = new CorridorNEW[numRooms*(numRooms)];

//        //~!The first corridor functionality is probably not necessary anymore.
//        //Create the first room and corridor:
//        //first time we make a room is unique because we're dropping it in the middle of the map and not building off of a previous room.
//        //rooms[0] = new RoomNEW();
//        //corridorsN[0] = new CorridorNEW();
//        //corridorsE[0] = new CorridorNEW();

//        //Setup the first room:
//        //rooms[0].SetupRoom(roomWidth, roomHeight, columns, rows);
//        //setup corridor using first room.
//        //corridorsN[0].SetupCorridor(rooms[0], corridorLength, roomWidth, roomHeight, columns, rows, true);
//        //corridorsE[0].SetupCorridor(rooms[0], corridorLength, roomWidth, roomHeight, columns, rows, true);


//        //loop over the length of rooms:
//        for (int i = 0; i < (rooms.Length-1)/numRooms; i++) 
//        {
//            for (int j = 0; j < (rooms.Length-1)/numRooms; j++)
//            {
//                rooms[i,j] = new global::RoomNEW();

//                rooms[i,j].SetupRoom(roomWidth, roomHeight, (i * roomWidth)+corridorLength, (j * roomHeight)+corridorLength, columns, rows);  //the new room is going to take the direction of the room we just created in mind when creating the room

//                if (j < corridorsN.Length)
//                {
//                    //create a corridor:
//                    corridorsN[j] = new global::CorridorNEW();
//                    corridorsE[j] = new global::CorridorNEW();
//                    corridorsN[j].SetupCorridor(rooms[i,j], DirectionNEW.North, corridorLength, roomWidth, roomHeight, columns, rows, false);
//                    corridorsE[j].SetupCorridor(rooms[i,j], DirectionNEW.East, corridorLength, roomWidth, roomHeight, columns, rows, false);

//                }

//                if (j == rooms.Length * .5)//halfway through the length of rooms, spawn the player (in a near-middle room)
//                {
//                    Vector3 playerPos = new Vector3(rooms[i,j].xPos, rooms[i,j].yPos, 0);
//                    Instantiate(player, playerPos, Quaternion.identity);
//                    InstantiateFoodSpawner(rooms[i,j]); //make a food spawner in the first room

//                }

//            }
//            //if we haven't reached the end of the corridors array...
//        }
//    }

//    void InstantiateFoodSpawner(RoomNEW room)
//    {
//        Vector2 foodSpawnCorner = new Vector2(room.xPos, room.yPos);
//        GameObject fs = Instantiate(foodSpawner, foodSpawnCorner, Quaternion.identity) as GameObject; //~~could change quaternion here for more variation?
//        fs.GetComponent<SpawnFood>().borderRight = room.roomWidth;
//        fs.GetComponent<SpawnFood>().borderTop = room.roomHeight;

//    }


//    //run through all room tiles and set them to be a floor:
//    void SetTilesValuesForRooms()
//    {
//        //many loops coming up.
//        //first, go to each room
//        for (int i = 0; i < rooms.Length; i++)
//        {

//            //then iterate through its x coordinates
//            for (int j = 0; j < rooms[i, j].roomWidth; j++)
//            {
//                RoomNEW currentRoom = rooms[i,j];

//                int xCoord = currentRoom.xPos + j;

//                //then go through each tile going upward:
//                for (int k = 0; k < currentRoom.roomHeight; k++)
//                {
//                    int yCoord = currentRoom.yPos + k;

//                    //set the coordinates in the jagged array to = floor tile
//                    tiles[xCoord][yCoord] = TileType.Floor;
//                    Debug.Log("tile type: " + xCoord + ", " + yCoord + " is a floor");

//                }
//            }
//        }
//    }

//    void SetTilesValuesForCorridors()
//    {
//        //now do the corridors:
//        for (int i = 0; i < corridorsN.Length; i++)
//        {
//            CorridorNEW currentCorridor = corridorsN[i];
//            //Debug.Log("CurrentCorridor Length = " + currentCorridor.corridorLength);
//            //go through its length:
//            for (int j = 0; j < currentCorridor.corridorLength; j++)
//            {
//                int xCoord = currentCorridor.startXPos;
//                int yCoord = currentCorridor.startYPos;

//                //depending on the direction, add or subtract from the appropriate coordinate based on how far through the length the loop is
//                switch (currentCorridor.direction)
//                {
//                    case DirectionNEW.North:
//                        yCoord += j;
//                        break;
//                    //case DirectionNEW.East:
//                    //    xCoord += j;
//                    //    break;
//                    //case DirectionNEW.South:
//                    //    yCoord -= j;
//                    //    break;
//                    //case DirectionNEW.West:
//                    //    xCoord -= j;
//                    //    break;
//                }
//                //Debug.Log("x: " + xCoord + " y: " + yCoord);

//                if (xCoord == currentCorridor.startXPos && yCoord == currentCorridor.startYPos) //the startpos of the corridor should be a door.
//                {
//                    tiles[xCoord][yCoord] = TileType.Door;
//                }
//                else
//                {
//                    tiles[xCoord][yCoord] = TileType.Floor;
//                }
//            }
//        }
//        for (int i = 0; i < corridorsE.Length; i++)
//        {
//            CorridorNEW currentCorridorE = corridorsE[i];

//            //go through its length:
//            for (int j = 0; j < currentCorridorE.corridorLength; j++)
//            {
//                int xCoord = currentCorridorE.startXPos2;
//                int yCoord = currentCorridorE.startYPos2;

//                //depending on the direction, add or subtract from the appropriate coordinate based on how far through the length the loop is
//                switch (currentCorridorE.direction)
//                {
//                    //case DirectionNEW.North:
//                    //    yCoord += j;
//                    //    break;
//                    case DirectionNEW.East:
//                        xCoord += j;
//                        break;
//                        //case DirectionNEW.South:
//                        //    yCoord -= j;
//                        //    break;
//                        //case DirectionNEW.West:
//                        //    xCoord -= j;
//                        //    break;
//                }
//                //Debug.Log("Ex: " + xCoord + " Ey: " + yCoord);

//                if (xCoord == currentCorridorE.startXPos2 && yCoord == currentCorridorE.startYPos2) //the startpos of the corridor should be a door.
//                {
//                    tiles[xCoord][yCoord] = TileType.Door;
//                }
//                else
//                {
//                    tiles[xCoord][yCoord] = TileType.Floor;
//                }
//            }
//        }

//    }

//    void InstantiateTiles()
//    {
//        for (int i = 0; i < tiles.Length; i++)
//        {
//            for (int j = 0; j < tiles[i].Length; j++)
//            {
//                //picks a random prefab from the floortiles array and instantiates it
//                //(everything gets a floor because atm the walls are destructable, so you'll need a floor underneath it)
//                InstantiateFromArray(floorTiles, i, j);
//                if (tiles[i][j] == TileType.Door)
//                {
//                    GameObject door = Instantiate(regularDoor, new Vector2(i, j), Quaternion.identity) as GameObject;
//                    door.transform.parent = boardHolder.transform;
//                }
//                if (tiles[i][j] == TileType.Wall) //if it's a wall, drop a wall on the floor
//                {
//                    InstantiateFromArray(wallTiles, i, j);
//                }
//            }
//        }
//    }

//    //~not sure how necessary this is for me but year:
//    void InstantiateOuterWalls()
//    {
//        //the outer walls are one unit left, right, up, and down from the board.
//        float leftEdgeX = -1f;
//        float rightEdgeX = columns + 0f;
//        float bottomEdgeY = -1f;
//        float topEdgeY = rows + 0f;

//        //instantiate both vertical walls:
//        InstantiateVerticalOuterWall(leftEdgeX, bottomEdgeY, topEdgeY);
//        InstantiateVerticalOuterWall(rightEdgeX, bottomEdgeY, topEdgeY);

//        //Instantiate both horizontal walls (in one unit left and right from outerwalls:
//        InstantiateHorizontalOuterWall(leftEdgeX + 1f, rightEdgeX - 1f, bottomEdgeY);
//        InstantiateHorizontalOuterWall(leftEdgeX + 1f, rightEdgeX - 1f, topEdgeY);
//    }

//    void InstantiateVerticalOuterWall(float xCoord, float startingY, float endingY)
//    {
//        float currentY = startingY;

//        //while the value for Y is less than the end value:
//        while (currentY <= endingY)
//        {
//            //instantiate outer wall tile at the x coord and the current y coord:
//            InstantiateFromArray(outerWallTiles, xCoord, currentY);

//            currentY++;
//        }
//    }

//    void InstantiateHorizontalOuterWall(float startingX, float endingX, float yCoord)
//    {
//        float currentX = startingX;

//        while (currentX <= endingX)
//        {
//            InstantiateFromArray(outerWallTiles, currentX, yCoord);
//            currentX++;
//        }
//    }

//    void InstantiateFromArray(GameObject[] prefabs, float xCoord, float yCoord)
//    {
//        int randomIndex = Random.Range(0, prefabs.Length);

//        Vector3 position = new Vector3(xCoord, yCoord, 0f); //not sure why this is a vector3 tbh.
//        GameObject tileInstance = Instantiate(prefabs[randomIndex], position, Quaternion.identity) as GameObject; //~~could change quaternion here for more variation?

//        tileInstance.transform.parent = boardHolder.transform; //store all the tiles in the board holder go
//    }
//}
