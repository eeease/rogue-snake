﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GridDungeonCreator : MonoBehaviour {
    public int numRoomsSquared = 3;
    public int roomWidth = 5;
    public int roomHeight = 5;
    public RoomNEW[,] rooms;
    private GameObject boardHolder, foodSpawnHolder;
    public static GridDungeonCreator GDC;
    public RoomGenerator.RoomType roomType;

    [Header("Prefabs")]
    public GameObject genericRoom;
    public GameObject player;
    public GameObject foodSpawner;
    public GameObject mainCamera;

    [Header("DebugSpawning")]
    public bool spawnSaw;
    public bool spawnSwitch;

    void Awake()
    {
        if (GDC == null)
            GDC = this;
        else if (GDC != this)
            Destroy(gameObject);

        //DontDestroyOnLoad(gameObject);

    }
    // Use this for initialization
    void Start () {
        //if (GameObject.Find("BoardHolder") == null)
        //{
        //    boardHolder = new GameObject("BoardHolder");
        //    boardHolder.AddComponent<BoardManager>();
        //}
        boardHolder = GameObject.Find("BoardHolder");
        foodSpawnHolder = GameObject.Find("FoodSpawnHolder");
        //doing this in the title start game button instead for now.  !~ probably not the best idea, as room creation could take a while, but we'll see.
        //CreateRooms();
    }

    // Update is called once per frame
    void Update () {
		
	}

    public void CreateRooms()
    {
        DestroyPreviousRooms();
        rooms = new RoomNEW[numRoomsSquared, numRoomsSquared];

        for (int i = 0; i< numRoomsSquared; i++)
        {
            for(int j=0; j<numRoomsSquared; j++)
            {
                rooms[i, j] = new global::RoomNEW();
                
                rooms[i, j].SetupRoom(roomWidth, roomHeight, (i * roomWidth), (j * roomHeight));  //the new room is going to take the direction of the room we just created in mind when creating the room

                GameObject room = Instantiate(genericRoom, new Vector2(rooms[i, j].xPos, rooms[i, j].yPos), Quaternion.identity) as GameObject;
                RoomGenerator rm = room.GetComponent<RoomGenerator>();
                int randRoom = Random.Range(0, 5);
                //Debug.Log(randRoom);
                rm.roomType = (RoomGenerator.RoomType)randRoom;
                //Debug.Log("my room type = " + rm.roomType);
                if (i == Mathf.RoundToInt(numRoomsSquared / 2) && j == Mathf.RoundToInt(numRoomsSquared / 2))
                {
                    //debug spawning:
                    if (spawnSaw)
                    {
                        rm.roomType = RoomGenerator.RoomType.Saw;
                        spawnSaw = false;
                    }
                    if (spawnSwitch)
                    {
                        rm.roomType = RoomGenerator.RoomType.Switches;
                        spawnSwitch = false;
                    }
                }
                rm.SetupRoom(roomWidth, roomHeight); //make it set itself up

                room.transform.parent = boardHolder.transform;

                //spawn the player, a food spawner, and the camera in the middle of the map:
                if (i==Mathf.RoundToInt(numRoomsSquared/2) && j == Mathf.RoundToInt(numRoomsSquared / 2))
                {
                    //make a food spawner in the first room always (right?~~)
                    
                    InstantiateFoodSpawner(rooms[i, j]);
                    
                    Instantiate(player, new Vector2(room.transform.position.x + Mathf.RoundToInt(roomWidth / 2), room.transform.position.y+Mathf.RoundToInt(roomHeight / 2)), Quaternion.identity);
                    InstantiateCam(rooms[i, j]);
                    
                }
            }
        }
    }

    void InstantiateFoodSpawner(RoomNEW room)
    {
        if (foodSpawnHolder.GetComponentInChildren<SpawnFood>() == null)
        {
            Vector2 foodSpawnCorner = new Vector2(room.xPos, room.yPos);
            GameObject fs = Instantiate(foodSpawner, foodSpawnCorner, Quaternion.identity) as GameObject; //~~could change quaternion here for more variation?
            fs.GetComponent<SpawnFood>().borderRight = room.roomWidth;
            fs.GetComponent<SpawnFood>().borderTop = room.roomHeight;
            fs.transform.parent = foodSpawnHolder.transform;
        }
    }

    void InstantiateCam(RoomNEW room)
    {
        Vector3 camMid = new Vector3(room.xPos+(room.roomWidth/2), room.yPos+(room.roomWidth / 2), -1);
        if (Camera.main == null)
        {
            GameObject mainCam = Instantiate(mainCamera, camMid, Quaternion.identity) as GameObject; //~~could change quaternion here for more variation?
        }
        else
        {
            //if there's already a camera (from a previous run) just set its pos:
            Camera.main.transform.position = camMid;
        }
        //mainCam.GetComponent<RoomTracker>().currentRoom = room.GetComponent<RoomGenerator>().roomID;
    }

    void DestroyPreviousRooms()
    {
        foreach (Transform genRoom in boardHolder.GetComponentsInChildren<Transform>())
        {
            if (genRoom.name != "BoardHolder") //don't destroy itself, cause, you know, Unity counts itself as a child.
            {
                Destroy(genRoom.gameObject);
            }
        }
        
    }

}
