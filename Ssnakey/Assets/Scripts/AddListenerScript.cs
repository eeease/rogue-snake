﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class AddListenerScript : MonoBehaviour {

	// Use this for initialization
	void Start () {
        
            gameObject.GetComponent<Button>().onClick.AddListener(() => GameManager.instance.MovePlayer(gameObject.name));

    }

}
