﻿using UnityEngine;

public enum DirectionNEW
{
    North, East, South, West,
}

public class CorridorNEW
{
    public int startXPos, startXPos2;
    public int startYPos, startYPos2;
    public int corridorLength;
    public DirectionNEW direction; //which direction the corridor is heading from it's room.
    public DirectionNEW direction2; //the second corridor should shoot which way.
    //ATM the planned functionality is that starting from bottom left, each room will sprout a corridor up and right.  That should lead to a grid full of accessible rooms.


    public int EndPositionX
    {
        get
        {
            if (direction == DirectionNEW.North || direction == DirectionNEW.South)
                return startXPos;
            if (direction == DirectionNEW.East)
                return startXPos + corridorLength - 1;
            return startXPos - corridorLength + 1;
        }
    }

    public int EndPositionY
    {
        get
        {
            if (direction == DirectionNEW.East || direction == DirectionNEW.West)
                return startYPos;
            if (direction == DirectionNEW.North)
                return startYPos + corridorLength - 1;
            return startYPos - corridorLength + 1; //must be South, so count down length of corridor, then add one to show where to start room
        }
    }

    public void SetupCorridor(RoomNEW room, DirectionNEW dir, int length, int roomWidth, int roomHeight, int columns, int rows, bool firstCorridor)
    {
        //Set a random direction (random index from 0 to 3, cast to Direction):
        //direction = (DirectionNEW)Random.Range(0, 4); //you can cast an int as an enum - super useful.
        direction = dir;
        /*Find the direction opposite to the one entering the room this corridor is leaving from.
         * Cast the previous corridor's direction to an int between 0 and 3, then add 2.
         * Find the remainder when dividing by 4 (if 2 then 2, if 3 then 3, if 4 then 0, if 5 then 1).
         * Cast this number back to a direction.
         * Overall effect is if the direction was South (2), that becomes 4, remainder when divided by 4 = 0, which is north.
         * */
        //ex. if the entering corridor came from the east, we add two to it to get west.  if we add one it'll be perpendicular.  THEN we're getting the remainder (if West + 2 = 5 /4, with a remainder of 1 (east))
        //DirectionNEW oppositeDirection = (DirectionNEW)(((int)room.enteringCorridor + 2) % 4);


        //if we're generating a corridor that's doubling back, instead turn perpendicular
        //if (!firstCorridor && direction == oppositeDirection)
        //{
        //    int directionInt = (int)direction;
        //    directionInt++;
        //    directionInt = directionInt % 4;
        //    direction = (DirectionNEW)directionInt;
        //}

        corridorLength = length; //~~this maybe should be changed.

        //Create a cap for how long the length can be (this will be changed based on the direction and position).
        int maxLength = length; //specified in BoardCreator

        //now, feed in which direction you want the corridor to go:
        switch (dir)
        {
            case DirectionNEW.North:
                //corridor can start at any xpos on the top of the room:
                startXPos = Random.Range(room.xPos, room.xPos + room.roomWidth - 1);
                startYPos = room.yPos + room.roomHeight;
                //the maximum length the corridor can be is the height of the board (rows) but from the top of the room (yPos+height)
                maxLength = rows - startYPos - roomHeight;

                break;

            case DirectionNEW.East:
                //Eastbound corridor:
                startXPos2 = room.xPos + room.roomWidth;
                startYPos2 = Random.Range(room.yPos, room.yPos + room.roomHeight - 1);
                maxLength = columns - startXPos - roomWidth;

                break;
        }



        //case DirectionNEW.South:
        //    //corridor can start at any xpos on the top of the room:
        //    startXPos = Random.Range(room.xPos, room.xPos + room.roomWidth);
        //    startYPos = room.yPos;
        //    //the maximum length the corridor can be is the height of the board (rows) but from the top of the room (yPos+height)
        //    maxLength = startYPos - roomHeight.m_Min;
        //    break;

        //case DirectionNEW.West:
        //    startXPos = room.xPos;
        //    startYPos = Random.Range(room.yPos, room.yPos + room.roomHeight);
        //    maxLength = startXPos - roomWidth.m_Min;
        //    break;
        //we clamp te length to make sure it doesn't go off the board.
        corridorLength = Mathf.Clamp(corridorLength, 1, maxLength);

    }



}




