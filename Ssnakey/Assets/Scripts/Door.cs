﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;


public class Door : MonoBehaviour
{
    public AudioClip[] openDoorSounds;              //1 of 2 audio clips that play when the wall is attacked by the player.
    public Sprite openSprite, closedSprite, lockedSprite;                   //Alternate sprite to display after Wall has been attacked by player or locked by higher powers.
    public IntRange tollrange = new IntRange(3, 10);                            //hit points for the wall.
    public int toll;

    public SpriteRenderer sr;      //Store a component reference to the attached SpriteRenderer.
    public Text myTollText;

    public bool isLocked; //if there's a switch puzzle in the room, ex. the door should start as being locked.

    void Awake()
    {
        //Get a component reference to the SpriteRenderer.
        sr = GetComponent<SpriteRenderer>();
        myTollText = GetComponentInChildren<Text>();
        toll = tollrange.Random;
        myTollText.text = toll.ToString();
    }


    //DamageWall is called when the player attacks a wall.
    public void OpenDoor(int incSegments)
    {
        if (incSegments >= toll) //if your snake has more segments than the door's toll:
        {
            //Call the RandomizeSfx function of SoundManager to play one of two chop sounds.
            SoundManager.instance.RandomizeSfx(openDoorSounds);

            //Set spriteRenderer to the damaged wall sprite.
            sr.sprite = openSprite;
            //Disable the gameObject.
            gameObject.SetActive(false);
        }
    }

    public void PayDoorToll(int incSegments)
    {
        if (incSegments > toll)
        {

        }

    }

    public void LockMe()
    {
        isLocked = true;
        sr.sprite = lockedSprite;
    }

    public void UnlockMe()
    {
        isLocked = false;
        sr.sprite = closedSprite;
    }
}

