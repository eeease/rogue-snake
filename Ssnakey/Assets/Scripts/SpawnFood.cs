﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//namespace Completed {
    public class SpawnFood : MonoBehaviour {
        public GameObject foodPrefab;
        public float repeatTime = 4; //~~later on this should probably be modified by GameManager (if you're spawning food quickly cause of a perk or something)
                                     //borders:
        public int borderTop, borderBottom, borderLeft, borderRight;
        // Use this for initialization
        void Start() {
            InvokeRepeating("Spawn", 3, repeatTime);
        
        }

    //// Update is called once per frame
    //void Update () {

    //}

    void Spawn()
    {
        if (gameObject.activeSelf)
        {
            //ints mean they won't spawn in between coordinates (1 instead of 1.222, ex.)
            //spawn them in between the bounds of the room that creates this
            int x = (int)Random.Range(transform.position.x + 1, transform.position.x + borderRight - 1);

            int y = (int)Random.Range(transform.position.y + 1, transform.position.y + borderTop - 1);

            Instantiate(foodPrefab, new Vector2(x, y), Quaternion.identity, gameObject.transform);
        }
    }
    }
//}
