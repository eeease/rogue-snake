﻿using System;

//this is a helper class to make things more concise so i don't have to type random.range so many times
[Serializable]
public class IntRange
{
    public int m_Min;
    public int m_Max;

    public IntRange(int min, int max)
    {
        m_Min = min;
        m_Max = max;
    }

    public int Random
    {
        get { return UnityEngine.Random.Range(m_Min, m_Max); }
    }
}
