﻿//using System.Collections;
//using System.Collections.Generic;
//using UnityEngine;

//public class BoardCreator : MonoBehaviour {
//    public enum TileType //first make them walls or floors or doors.
//    {
//        Wall, Floor, Door,
//    }

//    public int columns = 100; //total board size
//    public int rows = 100;
//    //helper class that generates random numbers.
//    public IntRange numRooms = new IntRange(15, 20);
//    public IntRange roomWidth = new IntRange(3, 10);
//    public IntRange roomHeight = new IntRange(3, 10);
//    public IntRange corridorLength = new IntRange(6, 10);
//    public GameObject[] floorTiles;
//    public GameObject[] wallTiles;
//    public GameObject[] outerWallTiles;
//    public GameObject regularDoor;
//    public GameObject foodSpawner;

//    private TileType[][] tiles; //a jagged array of TileTypes
//    private Room[] rooms;
//    private Corridor[] corridors;
//    private GameObject boardHolder;

//    public GameObject player;
//	// Use this for initialization
//	void Start () {

//        boardHolder = new GameObject("BoardHolder");

//        SetupNewBoard();

//	}

//    public void SetupNewBoard()
//    {
//        SetupTilesArray();

//        CreateRoomsAndCorridors();

//        SetTilesValuesForRooms();
//        SetTilesValuesForCorridors();

//        InstantiateTiles();
//        InstantiateOuterWalls();
//        if (GameManager.instance.levelImage.activeSelf)
//        {
//            GameManager.instance.levelImage.SetActive(false);
//        }
//    }

//    void SetupTilesArray()
//    {
//        //set the tiles jagged array to the correct width:
//        tiles = new TileType[columns][];

//        //go through all the tile arrays:
//        for(int i=0; i<tiles.Length; i++)
//        {
//            //set each tile array to correct height:
//            tiles[i] = new TileType[rows];
//        }
//    }

//    void CreateRoomsAndCorridors()
//    {
//        //Create the rooms array with a random size.
//        rooms = new Room[numRooms.Random]; //num rooms is an intrange, so saying .random means give me a number between 15 and 20 (in this ex)

//        //There should be one less corridor than there is rooms.
//        corridors = new Corridor[rooms.Length - 1]; //so if there are 4 rooms, you want three corridors (the fourth would lead to nowhere)

//        //Create the first room and corridor:
//        //first time we make a room is unique because we're dropping it in the middle of the map and not building off of a previous room.
//        rooms[0] = new Room();
//        corridors[0] = new Corridor();

//        //Setup the first room:
//        rooms[0].SetupRoom(roomWidth, roomHeight, columns, rows);
//        //setup corridor using first room.
//        corridors[0].SetupCorridor(rooms[0], corridorLength, roomWidth, roomHeight, columns, rows, true);

//        //loop over the length of rooms:
//        for(int i = 1; i<rooms.Length; i++) //start at 1 because 0 was created
//        {
//            rooms[i] = new global::Room();

//            rooms[i].SetupRoom(roomWidth, roomHeight, columns, rows, corridors[i - 1]);  //the new room is going to take the direction of the room we just created in mind when creating the room

//            //if we haven't reached the end of the corridors array...
//            if (i < corridors.Length) //one less than the number of rooms
//            {
//                //create a corridor:
//                corridors[i] = new global::Corridor();

//                corridors[i].SetupCorridor(rooms[i], corridorLength, roomWidth, roomHeight, columns, rows, false);
//            }

//            if (i == rooms.Length * .5)//halfway through the length of rooms, spawn the player (in a near-middle room)
//            {
//                Vector3 playerPos = new Vector3(rooms[i].xPos, rooms[i].yPos, 0);
//                Instantiate(player, playerPos, Quaternion.identity);
//                InstantiateFoodSpawner(rooms[i]); //make a food spawner in the first room

//            }
//        }
//    }

//    void InstantiateFoodSpawner(Room room)
//    {
//        Vector2 foodSpawnCorner = new Vector2(room.xPos, room.yPos);
//        GameObject fs = Instantiate(foodSpawner, foodSpawnCorner, Quaternion.identity) as GameObject; //~~could change quaternion here for more variation?
//        fs.GetComponent<SpawnFood>().borderRight = room.roomWidth; 
//        fs.GetComponent<SpawnFood>().borderTop = room.roomHeight;
        
//    }
	

//    //run through all room tiles and set them to be a floor:
//    void SetTilesValuesForRooms()
//    {
//        //many loops coming up.
//        //first, go to each room
//        for(int i = 0; i<rooms.Length; i++)
//        {
//            Room currentRoom = rooms[i];

//            //then iterate through its x coordinates
//            for (int j=0; j<currentRoom.roomWidth; j++)
//            {
//                int xCoord = currentRoom.xPos + j;

//                //then go through each tile going upward:
//                for(int k=0; k<currentRoom.roomHeight; k++)
//                {
//                    int yCoord = currentRoom.yPos + k;

//                    //set the coordinates in the jagged array to = floor tile
//                    tiles[xCoord][yCoord] = TileType.Floor;
//                }
//            }
//        }
//    }

//    void SetTilesValuesForCorridors()
//    {
//        //now do the corridors:
//        for (int i = 0; i < corridors.Length; i++)
//        {
//            Corridor currentCorridor = corridors[i];

//            //go through its length:
//            for(int j=0; j<currentCorridor.corridorLength; j++)
//            {
//                int xCoord = currentCorridor.startXPos;
//                int yCoord = currentCorridor.startYPos;

//                //depending on the direction, add or subtract from the appropriate coordinate based on how far through the length the loop is
//                switch (currentCorridor.direction)
//                {
//                    case Direction.North:
//                        yCoord += j;
//                        break;
//                    case Direction.East:
//                        xCoord += j;
//                        break;
//                    case Direction.South:
//                        yCoord -= j;
//                        break;
//                    case Direction.West:
//                        xCoord -= j;
//                        break;
//                }
//                Debug.Log("x: " + xCoord + " y: " + yCoord);

//                if (xCoord == currentCorridor.startXPos && yCoord == currentCorridor.startYPos) //the startpos of the corridor should be a door.
//                {
//                    tiles[xCoord][yCoord] = TileType.Door;
//                }
//                else
//                {
//                    tiles[xCoord][yCoord] = TileType.Floor;
//                }
//            }
//        }
//    }

//    void InstantiateTiles()
//    {
//        for(int i=0; i<tiles.Length; i++)
//        {
//            for(int j=0; j<tiles[i].Length; j++)
//            {
//                //picks a random prefab from the floortiles array and instantiates it
//                //(everything gets a floor because atm the walls are destructable, so you'll need a floor underneath it)
//                InstantiateFromArray(floorTiles, i, j);
//                if(tiles[i][j] == TileType.Door)
//                {
//                    GameObject door = Instantiate(regularDoor, new Vector2(i, j), Quaternion.identity) as GameObject;
//                    door.transform.parent = boardHolder.transform;
//                }
//                if(tiles[i][j] == TileType.Wall) //if it's a wall, drop a wall on the floor
//                {
//                    InstantiateFromArray(wallTiles, i, j);
//                }
//            }
//        }
//    }

//    //~not sure how necessary this is for me but year:
//    void InstantiateOuterWalls()
//    {
//        //the outer walls are one unit left, right, up, and down from the board.
//        float leftEdgeX = -1f;
//        float rightEdgeX = columns + 0f;
//        float bottomEdgeY = -1f;
//        float topEdgeY = rows + 0f;

//        //instantiate both vertical walls:
//        InstantiateVerticalOuterWall(leftEdgeX, bottomEdgeY, topEdgeY);
//        InstantiateVerticalOuterWall(rightEdgeX, bottomEdgeY, topEdgeY);

//        //Instantiate both horizontal walls (in one unit left and right from outerwalls:
//        InstantiateHorizontalOuterWall(leftEdgeX + 1f, rightEdgeX - 1f, bottomEdgeY);
//        InstantiateHorizontalOuterWall(leftEdgeX + 1f, rightEdgeX - 1f, topEdgeY);
//    }

//    void InstantiateVerticalOuterWall (float xCoord, float startingY, float endingY)
//    {
//        float currentY = startingY;

//        //while the value for Y is less than the end value:
//        while(currentY <= endingY)
//        {
//            //instantiate outer wall tile at the x coord and the current y coord:
//            InstantiateFromArray(outerWallTiles, xCoord, currentY);

//            currentY++;
//        }
//    }

//    void InstantiateHorizontalOuterWall (float startingX, float endingX, float yCoord)
//    {
//        float currentX = startingX;

//        while (currentX <= endingX)
//        {
//            InstantiateFromArray(outerWallTiles, currentX, yCoord);
//            currentX++;
//        }
//    }

//    void InstantiateFromArray (GameObject[] prefabs, float xCoord, float yCoord)
//    {
//        int randomIndex = Random.Range(0, prefabs.Length);

//        Vector3 position = new Vector3(xCoord, yCoord, 0f); //not sure why this is a vector3 tbh.

//        GameObject tileInstance = Instantiate(prefabs[randomIndex], position, Quaternion.identity) as GameObject; //~~could change quaternion here for more variation?

//        tileInstance.transform.parent = boardHolder.transform; //store all the tiles in the board holder go
//    }
//}
