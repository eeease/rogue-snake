﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using UnityEngine.UI;

public class SnakeControl : MonoBehaviour {
    public float speed = .3f; //this is how fast the snake will move (it'll move every 300ms at default)
    public Vector2 dir = Vector2.right;//default is moving to the right.
    public bool classicControls; //if true, player can use all arrow keys to move.  if false, left = rotate counterclockwise 90 degrees
    public List<Transform> tail = new List<Transform>();

    bool ate = false;
    public GameObject tailPrefab;
    public Text tailLength;

    [Header("Movement Modifier vars")]
    public bool infiniteBoost;
    public GameObject boostPrefab;
    public int numberOfBoosts, numberOfBoostsOG; //this can be upgraded over time so you can boost a bunch.
    public int boostIndex;
    bool canBoost, boostUnlocked;
    public float boostTimer;
    public float boostTimerOG = 2f;
    float xCoord = .7f;
    float yCoord = .6f;
    public List<SpriteRenderer> boostCDSprites = new List<SpriteRenderer>();
    public float moveDelay, moveDelayOG;
    public bool gogogo = false; //this should be false when a round starts and turned true when player presses a direction

    [Header("Door vars")]
    public float doorUnlockDelay;
    public Door currentDoorToUnlock;
    public bool unlockingDoor = false;


    // Use this for initialization
	void Start () {
        //InvokeRepeating("Move", 0, speed);
        
        //this will probably have to be changed but for now:
        tailLength = GameObject.Find("FoodText").GetComponent<Text>();
        tailLength.text = "Sssize:\n" + tail.Count.ToString();

        BoostSetup();

	}
	
	// Update is called once per frame
	void Update () {
        if (gogogo && GameManager.instance.inRun)
        {
            moveDelay -= Time.deltaTime;
            if (moveDelay <= 0)
            {
                Move();
                moveDelay = moveDelayOG;
            }
        }

        //Debug stuff:
        if (Input.GetKeyDown(KeyCode.F))
        {
            DropTheBomb();
        }



        if (boostIndex >0 && boostTimer > 0)
        {
            boostTimer -= Time.deltaTime;
        }
        //if boost is unlocked and the timer hits zero...
        if (boostUnlocked && boostTimer <= 0)
        {

                boostIndex--;
                boostCDSprites[boostIndex].enabled = true;
            
            //enable the sprite at boostindex (starts at 0)

            //if (boostIndex< boostCDSprites.Count-1) //if the index is less than the total
            //{
            //    boostIndex++;

            //}
            boostTimer = boostTimerOG;
            if (!canBoost)
            {
                canBoost = true;
            }


        }

        //may have to use an enum here for direction switch case

        if (Input.GetKeyDown(KeyCode.RightArrow))
        {
            MoveRight();
        }
        if (Input.GetKeyDown(KeyCode.DownArrow))
        {
            MoveDown();
        }
        if (Input.GetKeyDown(KeyCode.LeftArrow))
        {
            MoveLeft();
        }
        if (Input.GetKeyDown(KeyCode.UpArrow))
        {
            MoveUp();
        }



	}

    public void MoveRight()
    {
        dir = Vector2.right;
        moveDelay = 0;

        if (canBoost && dir == Vector2.right)
        {
            Move(); //get a little boost?
            ResetBoost();
        }
        if (!gogogo)
        {
            gogogo = true;
        }
    }
    public void MoveDown()
    {
        dir = Vector2.down;
        moveDelay = 0;

        if (canBoost && dir == Vector2.down)
        {
            Move();
            ResetBoost();
        }
        if (!gogogo)
        {
            gogogo = true;
        }

    }
    public void MoveLeft()
    {
        dir = Vector2.left;
        moveDelay = 0;

        if (canBoost && dir == Vector2.left)
        {
            Move();
            ResetBoost();
        }
        if (!gogogo)
        {
            gogogo = true;
        }
    }
    public void MoveUp()
    {
        dir = Vector2.up;
        moveDelay = 0;

        if (canBoost && dir == Vector2.up)
        {
            Move();
            ResetBoost();
        }
        if (!gogogo)
        {
            gogogo = true;
        }
    }

    void BoostSetup()
    {
        numberOfBoosts = numberOfBoostsOG; //set it to your max number of boosts.
        boostIndex = 0;
        if (numberOfBoostsOG > 0)
        {
            boostUnlocked = true; //allow snake to start being able to boost if it has been unlocked.
            canBoost = true;
        }
        else
        {
            boostUnlocked = false;
        }
        //create a boost eyeball for each boost that you have
        while (numberOfBoosts > 0)
        {
            Vector2 boostPos = new Vector2(xCoord, yCoord);
            GameObject boostDot = Instantiate(boostPrefab, gameObject.transform) as GameObject;
            boostDot.transform.localPosition = boostPos;
            boostCDSprites.Add(boostDot.GetComponent<SpriteRenderer>());
            xCoord -= .3f;
            numberOfBoosts--;
        }

    }

    void Move()
    {
        if (!unlockingDoor && !GameManager.instance.runOver)
        {
            //get position of upcoming gap before moving head:
            Vector2 gapPos = transform.position;
            //move to next spot:
            transform.Translate(dir / 2); //divided by 2 to make the board seem larger (if it starts in the bottom left of a unit, it can move right, up, and left within the same unit)
            if (ate)
            {
                //load prefab into world:
                GameObject g = Instantiate(tailPrefab, gapPos, Quaternion.identity);
                //add it to our list:
                tail.Insert(0, g.transform);
                g.GetComponent<TailControl>().myHead = GetComponent<SnakeControl>();
                TailIndexAssign();
                tailLength.text = "Sssize:\n" + tail.Count.ToString();
                //update score vars:
                GameManager.instance.segmentsConsumed++;
                TailMaxLengthCheck(tail.Count);
                ate = false;

            }
            //check tail and move accordingly:
            else if (tail.Count > 0)
            {
                //move last tail element to where head just was:
                tail.Last().position = gapPos;

                //Add to front of list, remove from the back:
                tail.Insert(0, tail.Last());
                tail.RemoveAt(tail.Count - 1);
                TailIndexAssign();
                tailLength.text = "Sssize:\n" + tail.Count.ToString();

            }

        }

    }

    void TailMaxLengthCheck(int currentLength)
    {
        if(currentLength > GameManager.instance.maxLength)
        {
            GameManager.instance.maxLength = currentLength;
        }
    }
    void TailIndexAssign()
    {
        foreach (Transform tc in tail)
        {
            tc.GetComponent<TailControl>().myTailIndex = tail.IndexOf(tc);
        }
    }

    void ResetBoost()
    {
        if (!infiniteBoost)
        {
            boostCDSprites[boostIndex].enabled = false; //turn off the visual cue of boost.

            if (boostIndex >= boostCDSprites.Count - 1)
            {
                canBoost = false;

            }

            boostIndex++;

            boostTimer = boostTimerOG;
        }
    }

    void OnTriggerEnter2D(Collider2D col)
    {
        switch (col.tag)
        {
            case "Food":
                //while ate = true, it'll get longer next Move call instead of shuffling order
                ate = true;
                Destroy(col.gameObject);
                break;
            case "Enemy":
            case "Saw":
//fall through to tail logic (end run)
            case "Tail":
                GameManager.instance.EndRun();
                Destroy(gameObject);
                break;
            //for now, the wall is going to end the run.  in the future, i think you should be able to pay 1 segment per wall piece to burrow through it?  maybe you can mine for stuff in the walls like gems?
            case "Wall":
                GameManager.instance.EndRun();
                Destroy(gameObject);

                break;

            case "Door":
                //store instance of the currentDoor:
                currentDoorToUnlock = col.GetComponent<Door>();
                StartCoroutine(PayWithLastTailSegmentCR(currentDoorToUnlock));
                unlockingDoor = true;
                //PayWithLastTailSegment(currentDoorToUnlock);
                //col.GetComponent<Door>().OpenDoor(tail.Count);
                break;
        }
    }


    void UnlockDoor()
    {
        InvokeRepeating("PayWithLastTailSegment", 0, doorUnlockDelay);
    }

    public void GhostTails()
    {
        foreach(Transform t in tail)
        {
            t.GetComponent<TailControl>().GhostLikeSwayze();
        }
    }
    /// <summary>
    /// feed this your tail number in the list and cut off all following tails.
    /// </summary>
    /// <param name="index"></param>
    public void GhostTails(int index)
    {
        gogogo = false; //pause movement until tail parts are cut off?
        if (index < tail.Count)
        {
            for (int i = index; i < tail.Count; i++)
            {
                tail[i].GetComponent<TailControl>().GhostLikeSwayze();

                tail.RemoveAt(i);
            }
            gogogo = true;
        }
        else
        {
            tail[index].GetComponent<TailControl>().GhostLikeSwayze();

            tail.RemoveAt(index);
            gogogo = true;
        }
    }

    IEnumerator PayWithLastTailSegmentCR(Door door)
    {
        if (!door.isLocked)
        {
            Vector2 tempDir = dir;
            dir = Vector2.zero; //stop movement.
                                //loop through and destroy a tail segment (and decrement the door toll).
            for (int i = tail.Count; i > -1; i--)
            {
                if (i == 0)
                {
                    Destroy(gameObject);
                    GameManager.instance.EndRun();
                    yield break;
                }
                //Debug.Log("current toll = " + currentDoorToUnlock.toll);
                Destroy(tail[i - 1].gameObject);
                tail.RemoveAt(i - 1);
                currentDoorToUnlock.toll--;

                //check if the door toll is paid.  if so, change door sprite and get out of loop
                if (door.toll <= 0)
                {
                    door.sr.sprite = door.openSprite;
                    //Debug.Log("You unlocked it");
                    door.GetComponent<BoxCollider2D>().enabled = false;
                    unlockingDoor = false;
                    dir = tempDir;
                    yield break;
                }
                tailLength.text = "Sssize:\n" + tail.Count.ToString();

                yield return new WaitForSeconds(doorUnlockDelay);

            }
        }else
        {
            Destroy(gameObject);
            GameManager.instance.EndRun();
        }
    }
    public void DropTheBomb()
    {
        if (tail.Count > 0)
        {
            //it should drop the tail which then explodes or blinks away?
            tail[tail.Count - 1].GetComponent<TailControl>().isBomb = true;
            tail.RemoveAt(tail.Count - 1);
        }

    }
}
