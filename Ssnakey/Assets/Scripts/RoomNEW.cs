﻿using UnityEngine;

public class RoomNEW
{
    public int xPos; //x coordinate of the lower left tile in the room.
    public int yPos; //see above
    public int roomWidth; //how many tiles wide the room is.
    public int roomHeight;
    public DirectionNEW enteringCorridor; //the direction of the corridor that is entering this room.

    //this one is only used for the first room.
    public void SetupRoom(int widthRange, int heightRange, int xPoss, int yPoss, int columns, int rows)
    {
        //Set a random width and height:
        roomWidth = widthRange;
        roomHeight = heightRange;

        //Set the x and y coordinates so the room is roughly in the middle of the board:
        xPos = xPoss;
        yPos = yPoss;
        //Debug.Log("x: " + xPos + " y: " + yPos);
    }

    public void SetupRoom(int widthRange, int heightRange, int xPoss, int yPoss)
    {
        //Set a random width and height:
        roomWidth = widthRange;
        roomHeight = heightRange;

        //Set the x and y coordinates so the room is roughly in the middle of the board:
        xPos = xPoss;
        yPos = yPoss;
        //Debug.Log("x: " + xPos + " y: " + yPos);
    }
    //this overloaded method is for subsequent rooms:
    //public void SetupRoom(IntRange widthRange, IntRange heightRange, int columns, int rows)
    //{
    //    //set the entering corridor direction:
    //    //enteringCorridor = corridor.direction;

    //    //Set a random width and height:
    //    roomWidth = widthRange.Random;
    //    roomHeight = heightRange.Random;


    //    //similar to the corridor switch:
    //    //switch (corridor.direction)
    //    //{
    //    //    //!!something in here is incorrect and is throwing an array error in corridor.  EX. the corridor was trying to draw at -5, 45, which is out of index)
    //    //    case DirectionNEW.North:
    //    //        //the height of the room can't go beyond the board so it must be clamped based on the height of the board (rows) and the end of the corridor that leads to the room.
    //    //        roomHeight = Mathf.Clamp(roomHeight, 1, rows - corridor.EndPositionY); //check corridor ~~ line 31

    //    //        yPos = corridor.EndPositionY;

    //    //        //so the start the of room isn't too far to the left/right of the end of the corridor.
    //    //        xPos = Random.Range(corridor.EndPositionX - roomWidth + 1, corridor.EndPositionX);

    //    //        xPos = Mathf.Clamp(xPos, 0, columns - roomWidth);
    //    //        break;

    //    //    case DirectionNEW.East:
    //    //        //the height of the room can't go beyond the board so it must be clamped based on the height of the board (rows) and the end of the corridor that leads to the room.
    //    //        roomWidth = Mathf.Clamp(roomWidth, 1, columns - corridor.EndPositionX); //check corridor ~~ line 31

    //    //        xPos = corridor.EndPositionX;
    //    //        //so the start the of room isn't too far to the north/south of the end of the corridor.
    //    //        yPos = Random.Range(corridor.EndPositionY - roomHeight + 1, corridor.EndPositionY);
    //    //        yPos = Mathf.Clamp(yPos, 0, rows - roomHeight);
    //    //        break;

    //    //    case DirectionNEW.South:
    //    //        roomHeight = Mathf.Clamp(roomHeight, 1, corridor.EndPositionY); //check corridor ~~ line 31
    //    //        yPos = corridor.EndPositionY - roomHeight+1;
    //    //        xPos = Random.Range(corridor.EndPositionX - roomWidth + 1, corridor.EndPositionX);
    //    //        xPos = Mathf.Clamp(xPos, 0, columns - roomWidth);
    //    //        break;

    //    //    case DirectionNEW.West:
    //    //        roomWidth = Mathf.Clamp(roomWidth, 1, corridor.EndPositionX); //check corridor ~~ line 31

    //    //        xPos = corridor.EndPositionX - roomWidth+1;
    //    //        yPos = Random.Range(corridor.EndPositionY - roomHeight + 1, corridor.EndPositionY);
    //    //        yPos = Mathf.Clamp(yPos, 0, rows - roomHeight);
    //    //        break;



    //    //}
    //}



}
