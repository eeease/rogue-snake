﻿using UnityEngine;

public enum Direction
{
    North, East, South, West,
}

public class Corridor
{
    public int startXPos; 
    public int startYPos;
    public int corridorLength;
    public Direction direction; //which direction the corridor is heading from it's room.


    public int EndPositionX
    {
        get
        {
            if (direction == Direction.North || direction == Direction.South)
                return startXPos;
            if (direction == Direction.East)
                return startXPos + corridorLength - 1;
            return startXPos = corridorLength + 1;
        }
    }

    public int EndPositionY
    {
        get
        {
            if (direction == Direction.East || direction == Direction.West)
                return startYPos;
            if (direction == Direction.North)
                return startYPos + corridorLength - 1;
            return startYPos - corridorLength + 1; //must be South, so count down length of corridor, then add one to show where to start room
        }
    }

    public void SetupCorridor (Room room, IntRange length, IntRange roomWidth, IntRange roomHeight, int columns, int rows, bool firstCorridor)
    {
        //Set a random direction (random index from 0 to 3, cast to Direction):
        direction = (Direction)Random.Range(0, 4); //you can cast an int as an enum - super useful.
        /*Find the direction opposite to the one entering the room this corridor is leaving from.
         * Cast the previous corridor's direction to an int between 0 and 3, then add 2.
         * Find the remainder when dividing by 4 (if 2 then 2, if 3 then 3, if 4 then 0, if 5 then 1).
         * Cast this number back to a direction.
         * Overall effect is if the direction was South (2), that becomes 4, remainder when divided by 4 = 0, which is north.
         * 
         * 
         * 
         * */
        //ex. if the entering corridor came from the east, we add two to it to get west.  if we add one it'll be perpendicular.  THEN we're getting the remainder (if West + 2 = 5 /4, with a remainder of 1 (east))
        Direction oppositeDirection = (Direction)(((int)room.enteringCorridor + 2) % 4);


        //if we're generating a corridor that's doubling back, instead turn perpendicular
        if(!firstCorridor && direction == oppositeDirection)
        {
            int directionInt = (int)direction;
            directionInt++;
            directionInt = directionInt % 4;
            direction = (Direction)directionInt;
        }

        corridorLength = length.Random; //~~this maybe should be changed.

        //Create a cap for how long the length can be (this will be changed based on the direction and position).
        int maxLength = length.m_Max; //specified in BoardCreator

        switch (direction)
        {
            case Direction.North:
                //corridor can start at any xpos on the top of the room:
                startXPos = Random.Range(room.xPos, room.xPos + room.roomWidth - 1);
                startYPos = room.yPos + room.roomHeight;
                //the maximum length the corridor can be is the height of the board (rows) but from the top of the room (yPos+height)
                maxLength = rows - startYPos - roomHeight.m_Min;
                break;

            case Direction.East:
                startXPos = room.xPos + room.roomWidth;
                startYPos = Random.Range(room.yPos, room.yPos + room.roomHeight - 1);
                maxLength = columns - startXPos - roomWidth.m_Min;
                break;

            case Direction.South:
                //corridor can start at any xpos on the top of the room:
                startXPos = Random.Range(room.xPos, room.xPos + room.roomWidth);
                startYPos = room.yPos;
                //the maximum length the corridor can be is the height of the board (rows) but from the top of the room (yPos+height)
                maxLength = startYPos - roomHeight.m_Min;
                break;

            case Direction.West:
                startXPos = room.xPos;
                startYPos = Random.Range(room.yPos, room.yPos + room.roomHeight);
                maxLength = startXPos - roomWidth.m_Min;
                break;

        }

        //we clamp te length to make sure it doesn't go off the board.
        corridorLength = Mathf.Clamp(corridorLength, 1, maxLength);

    }
}


