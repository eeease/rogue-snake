﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;


	using System.Collections.Generic;		//Allows us to use Lists. 
	using UnityEngine.UI;                   //Allows us to use UI.

public class GameManager : MonoBehaviour
{
    public static GameManager instance = null;              //Static instance of GameManager which allows it to be accessed by any other script.

    public enum ShotType
    {
        Fire, Ice, Laser, Regular,
    }

    public int currentLevel = 1;                                  //Current level number, expressed in game as "Day 1".
    private bool doingSetup = true;                         //Boolean to check if we're setting up board, prevent Player from moving during setup.
    public bool runOver;
    public bool inRun;
    public bool inTitle;
    public float endRunScoreDelay = 3;

    [Header("RoomsInfo/ScoreStats")]
    public int totalNumberOfRooms, roomsDiscovered;
    public int maxLength, segmentsConsumed;
    public int totalScore;
    public int numOfFood, numOfSwitches, numOfBosses, numOfMazes, numOfShops; //this will come in handy when 'balancing' a floor to have a variety of rooms.

    public SnakeControl player; //it's always nice to have an instance of the player...
    public List<GameObject> ghostedTails;
    public GameObject swipeManagerGO;

    //Awake is always called before any Start functions
    void Awake()
    {
        //Check if instance already exists
        if (instance == null)

            //if not, set instance to this
            instance = this;

        //If instance already exists and it's not this:
        else if (instance != this)

            //Then destroy this. This enforces our singleton pattern, meaning there can only ever be one instance of a GameManager.
            Destroy(gameObject);

        //Sets this to not be destroyed when reloading scene
        DontDestroyOnLoad(gameObject);
        foreach(Transform t in GetComponentsInChildren<Transform>())
        {
            if(t.name == "SwipeManager")
            {
                swipeManagerGO = t.gameObject;

            }
        }

    }

    void Start()
    {
    }


    //Update is called every frame.
    void Update()
    {
        if (runOver && Input.GetButtonDown("Restart"))
        {
            RestartGame();
        }
        if (!inTitle && !runOver && player == null)
        {
            FindPlayer();
        }
        //Check that playersTurn or enemiesMoving or doingSetup are not currently true.
        
 
    }



    public void EndRun()
    {
        SetGameStateBools(2);

        Invoke("MoveToScore", endRunScoreDelay); //have to indirectly invoke this since it takes a parameter
    }

    public void MoveToScore()
    {
        player.GhostTails(); //set all tails to ghost mode
        UIManager.UI.LerpPanel(2);
        //Destroy(player.gameObject);
    }

    public void TrashGhostTails()
    {
        foreach(GameObject go in ghostedTails)
        {
            Destroy(go);
        }
        ghostedTails.Clear();
    }

    public void RestartGame()
    {
        UIManager.UI.LerpPanel(4);
        SetGameStateBools(1);
        GridDungeonCreator.GDC.CreateRooms();
        ResetScore();
        //GetComponent<BoardCreator>().SetupNewBoard();
    }

    public void ResetScore()
    {
        maxLength = 0;
        roomsDiscovered = 0;
        totalScore = 0;
        segmentsConsumed = 0;
        
    }

    public void MovePlayer(string name)
    {
        if (player != null)
        {
            switch (name)
            {
                case "2":
                    player.MoveUp();
                    break;
                case "4":
                    player.MoveLeft();
                    break;
                case "6":
                    player.MoveRight();
                    break;
                case "8":
                    player.MoveDown();
                    break;


            }
        }
    }
    public void PlayerTailBomb()
    {
        player.DropTheBomb();
    }
    public void FindPlayer()
    {
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<SnakeControl>();
    }

    //this will decide how to lerp based on what state the game is currently in
    public void StartARun()
    {
        if (runOver)
        {
            UIManager.UI.LerpPanel(4); //load the togamefromscore anim
        }
        else if (inTitle) //else if you're coming from the title screen:
        {
            UIManager.UI.LerpPanel(1);
        }
        SetGameStateBools(1);
        ResetScore();
    }

    /// <summary>
    /// 0=inTitle; 1=inRun; 2=inScore
    /// </summary>
    /// <param name="whichState"></param>
    public void SetGameStateBools(int whichState)
    {
        switch (whichState)
        {
            case 0:
                inTitle = true;
                inRun = false;
                runOver = false;
                break;

            case 1:
                inTitle = false;
                inRun = true;
                runOver = false;

                break;

            case 2:
                inTitle = false;
                inRun = false;
                runOver = true;

                break;
        }
    }


    //a debug function to turn swiping on or off
    public void SwapSwipe()
    {
        if (swipeManagerGO.activeSelf)
        {
            swipeManagerGO.SetActive(false);
            UIManager.UI.swipeButtText.text = "Swipe\nOff";
        }else
        {
            swipeManagerGO.SetActive(true);
            UIManager.UI.swipeButtText.text = "Swipe\nOn";

        }
    }


}

