﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TileRandomizer : MonoBehaviour {
    SpriteRenderer mySR;
    public Sprite[] sprites;

    //public bool camTriggerTile;
	// Use this for initialization
	void Start () {
        mySR = GetComponent<SpriteRenderer>();
        mySR.sprite = sprites[Random.Range(0, sprites.Length)];
        //if (camTriggerTile)
        //{
        //    tag = "CamTrigger";
        //    gameObject.AddComponent<Collider2D>();
        //    GetComponent<Collider2D>().isTrigger = true;
        //}
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
