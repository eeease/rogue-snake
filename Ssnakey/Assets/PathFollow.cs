﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PathFollow : MonoBehaviour {
    public PathObj[] pathPoints;
    public int objIndex = 0;
    public float moveSpeed, coolDown, coolDownOG;
    public float distanceToReset = 0.1f;
    public Transform path;
    public bool reenableCollider = false;

	// Use this for initialization
	void Start () {
        
		
	}
	
	// Update is called once per frame
	void Update () {
        MoveToCurrentPoint();
        coolDown -= Time.deltaTime;
        if (coolDown <= 0)
        {
            MoveToNextPoint();
        }
        
	}

    public void AssignPathPoints()
    {
        pathPoints = path.GetComponentsInChildren<PathObj>();

    }

    void MoveToNextPoint()
    {
        if (objIndex < pathPoints.Length - 1)
        {
            objIndex++;
        }else
        {
            objIndex = 0;
        }
        //if you just hit a tail, this will be set to true and the collider will come back at the next stopping point.
        if (reenableCollider)
        {
            GetComponent<Collider2D>().enabled = true;
            reenableCollider = false;
        }
        coolDown = coolDownOG;
    }

    void MoveToCurrentPoint()
    {
        transform.position = Vector2.Lerp(transform.position, pathPoints[objIndex].transform.position, moveSpeed * Time.deltaTime);

    }
}
