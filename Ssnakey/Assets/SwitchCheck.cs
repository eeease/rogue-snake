﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//a small helper class that'll check the childed switch tiles to see if they're all on
public class SwitchCheck : MonoBehaviour {
    public SwitchPatternScript[] childedSwitches;
    public int numOfSwitches, numOfOnSwitches;
	// Use this for initialization
	void Start () {
        childedSwitches = GetComponentsInChildren<SwitchPatternScript>();
        numOfSwitches = childedSwitches.Length;

	}
	
	// Update is called once per frame
	void Update () {
		
	}

    //instead of checking for this in update, just fire a check each time the player switches on a... switch (it's late. i'm tired)
    //!~This needs to be tightened up.  It's currently inconsistent.
    public void CheckIfAllOn()
    {
        numOfOnSwitches = 0;
        for(int i=0; i<childedSwitches.Length; i++)
        {
            if (childedSwitches[i].imOn)
            {
                numOfOnSwitches++;
            }
        }
        if (numOfOnSwitches >= numOfSwitches-1)
        {
            Debug.Log("great job, room thing happen!");
            GetComponentInParent<RoomGenerator>().UnlockDoors();
        }
        
    }
}
